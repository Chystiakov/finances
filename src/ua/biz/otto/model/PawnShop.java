package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Loan;

public class PawnShop extends Organization implements Loan {

    public static final double INTEREST = 0.4;
    double returnAmount;

    public PawnShop(String name, String address) {
        super(name, address);
    }

    @Override
    public double loan(int amount) {
        if (amount > 50000) {
            System.out.println("Loan declined");
        } else {
            System.out.println("Loan accepted");
        }
        return INTEREST;

    }


}

