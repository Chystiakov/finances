package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Sender;

public class SendTest extends Organization implements Sender {
    public SendTest(String name, String address) {
        super(name, address);
    }

    @Override
    public double send(int uah) {
        double sendFee = uah * 0.01;
        return sendFee;
}
}
