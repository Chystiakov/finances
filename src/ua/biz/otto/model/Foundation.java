package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Deposit;

public class Foundation extends Organization implements Deposit {

    int year;

    public Foundation(String name, String address, int year) {
        super(name, address);
        this.year = year;
    }

    @Override
    public void deposit(int time, int amount) {
        if (time >= 12) {
            System.out.println("Deposit accepted: " + amount);
        } else {
            System.out.println("Deposit not accepted: " + amount);
        }
    }
}
