package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Sender;

public class Post extends Organization implements Sender {
    public Post(String name, String address) {
        super(name, address);
    }

    @Override
    public double send(int uah) {
        double sendFee = uah * 0.02;
        return sendFee;
    }
}
