package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Loan;

public class CreditCafe extends Organization implements Loan {

    public static final double INTEREST = 2;
    double returnAmount;

    public CreditCafe(String name, String address) {
        super(name, address);
    }

    @Override
    public double loan(int amount) {
        if (amount > 4000) {
            System.out.println("Loan declined");
        } else {
            System.out.println("Loan accepted");
        }
        return INTEREST;

    }


}
