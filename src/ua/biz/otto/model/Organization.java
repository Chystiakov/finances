package ua.biz.otto.model;

public class Organization {
    String name;
    String address;

    public Organization(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}
