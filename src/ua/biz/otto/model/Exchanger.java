package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Exchange;

public class Exchanger extends Organization implements Exchange {

    double[] rates;
    double result;

    public Exchanger(String name, String address, double[] rates) {
        super(name, address);
        this.rates = rates;
    }

    @Override
    public double convert(int currency, int amount) {
        switch (currency) {
            case 0:
                result = amount * rates[currency];
                break;
            case 1:
                result = amount * rates[currency];
                break;
            case 2:
                result = amount * rates[currency];
                break;
            case 3:
                result = amount / rates[currency - 3];
                break;
            case 4:
                result = amount / rates[currency - 3];
                break;
            case 5:
                result = amount / rates[currency - 3];
                break;
        }

        return result;

    }
}

