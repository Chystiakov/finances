package ua.biz.otto.model;

import ua.biz.otto.Interfaces.Deposit;
import ua.biz.otto.Interfaces.Exchange;
import ua.biz.otto.Interfaces.Loan;
import ua.biz.otto.Interfaces.Sender;

public class Bank extends Organization implements Exchange, Sender, Deposit, Loan {

    final static int LIMIT = 12000;
    final static int COMISSION = 15;
    int year;
    double[] rates;
    double result;
    public static final double INTEREST = 0.25;
    double returnAmount;

    public Bank(String name, String address, int year, double[] rates) {
        super(name, address);
        this.year = year;
        this.rates = rates;
    }

    @Override
    public double convert(int currency, int amount) {
        switch (currency) {
            case 0:
            case 1:
            case 2:
                if (amount * rates[currency] - COMISSION > LIMIT) {
                    result = 0;
                } else {
                    result = amount * rates[currency] - COMISSION;
                }
                break;
            case 3:
                if (amount > LIMIT) {
                    result = 0;
                } else {
                    result = amount / rates[currency - 3] - COMISSION;
                }
                break;
            case 4:
                if (amount > LIMIT) {
                    result = 0;
                } else {
                    result = amount / rates[currency - 3] - COMISSION;
                }
                break;
            case 5:
                if (amount > LIMIT) {
                    result = 0;
                } else {
                    result = amount / rates[currency - 3] - COMISSION;
                }
                break;
        }
        return result;
    }


    @Override
    public double send(int uah) {
        double sendFee = uah * 0.01 + 5;
        return sendFee;
    }

    @Override
    public void deposit(int time, int amount) {
        if (time <= 12) {
            System.out.println("Deposit accepted: " + amount);
        } else {
            System.out.println("Deposit not accepted");
        }
    }

    @Override
    public double loan(int amount) {
        if (amount > 200000) {
            System.out.println("Loan declined");
        } else {
            System.out.println("Loan accepted");
    }
        return INTEREST;


    }
}
