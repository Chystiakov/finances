package ua.biz.otto;

import ua.biz.otto.Interfaces.Deposit;
import ua.biz.otto.Interfaces.Exchange;
import ua.biz.otto.Interfaces.Loan;
import ua.biz.otto.Interfaces.Sender;
import ua.biz.otto.model.*;

import java.util.Scanner;
import java.util.Vector;

public class FinanceSystem {
    Vector<Organization> organizations;

    int currency = 0;
    int amount = 0;


    public FinanceSystem(Vector<Organization> organizations) {
        this.organizations = organizations;
    }

    Scanner scanner = new Scanner(System.in);

    public void operate() {

        // Enter operation
        System.out.println("Choose operation: \n" +
                "0. Convert\n" +
                "1. Send\n" +
                "2. Deposit\n" +
                "3. Loan\n");
        int operation = scanner.nextInt();

        switch (operation) {
            case 0:
                // Enter way to convert
                System.out.println("Choose currency: \n" +
                        "0. USD to UAH\n" +
                        "1. EUR to UAH\n" +
                        "2. RUB to UAH\n" +
                        "3. UAH to USD\n" +
                        "4. UAH to EUR\n" +
                        "5. UAH to RUB\n");
                currency = scanner.nextInt();

                //Enter amount of money to convert
                System.out.println("Enter the amount to convert: ");
                amount = scanner.nextInt();

                for (Organization organization : organizations) {
                    if (organization instanceof Exchange) {
                        System.out.println(organization.getName());
                        System.out.println(organization.getAddress());
                        double value = ((Exchange) organization).convert(currency, amount);
                        if (value == 0) {
                            System.out.println("Overlimit\n");
                        } else {
                            System.out.println(value + "\n");
                        }
                    }
                }
                printBestExchanger();
                break;
            case 1:
                // Amount to send
                System.out.println("Amount to send: \n");
                int sendAmount = scanner.nextInt();
                for (Organization organization : organizations) {
                    if (organization instanceof Sender) {
                        System.out.println(organization.getName());
                        System.out.println(organization.getAddress());
                        System.out.println(sendAmount);
                        System.out.println("Send fee:" + ((Sender) organization).send(sendAmount));
                    }
                }
                break;
            case 2:
                //Deposit duration and amount
                System.out.println("Input deposit duration in months: ");
                int depDuration = scanner.nextInt();
                System.out.println("Input deposit amount: ");
                int depAmount = scanner.nextInt();
                for (Organization organization : organizations) {
                    if (organization instanceof Deposit) {
                        System.out.println(organization.getName());
                        System.out.println(organization.getAddress());
                        ((Deposit) organization).deposit(depDuration, depAmount);
                    }
                }
                break;
            case 3:
                //Loan amount
                System.out.println("Enter loan amount needed: ");
                int loanAmount = scanner.nextInt();
                for (Organization organization : organizations) {
                    if (organization instanceof Loan) {
                        System.out.println(organization.getName());
                        System.out.println(organization.getAddress());
                        System.out.println(((Loan) organization).loan(loanAmount));
                    }
                }
                break;
        }
    }

    public void printBestExchanger() {
        Organization bestExchanger = null;
        double bestValue = 0;
        for (Organization organization : organizations) {
            if (organization instanceof Exchange) {
                double value = ((Exchange) organization).convert(currency, amount);
                if (value > bestValue) {
                    bestExchanger = organization;
                    bestValue = value;
                }
            }
        }
        System.out.println("Best Exchanger: " + bestExchanger.getName());
    }

}

