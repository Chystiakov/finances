package ua.biz.otto;

import ua.biz.otto.model.*;

import java.util.Vector;

public class Generator {
    private Generator() {
    }


    public static Vector<Organization> generate() {
        Vector<Organization> organizations = new Vector();
        organizations.add(new Bank("Bank", "Sumskaya", 1998, new double[]{26.60, 32.95, 0.47}));
        organizations.add(new Post("Post", "Pravdy"));
        organizations.add(new Exchanger("Exchanger", "Lenina", new double[]{26.70, 32.90, 0.46}));
        organizations.add(new Foundation("PIF", "Bakulina", 2000));
        organizations.add(new CreditUnion("CreditUnion", "Vesnina"));
        organizations.add(new CreditCafe("CreditCafe", "Beketova"));
        organizations.add(new PawnShop("PawnShop", "Pobedy"));
        organizations.add(new SendTest("SendTest", "Test"));

        return organizations;
    }
}
   /* public static Organization[] generate() {
        Organization organizations[] = new Organization[7];
        organizations[0] = new Bank("Bank", "Sumskaya", 1998, new double[]{26.60, 32.95, 0.47});
        organizations[1] = new Post("Post", "Pravdy");
        organizations[2] = new Exchanger("Obmenka", "Lenina", new double[]{26.70, 32.90, 0.46});
        organizations[3] = new Foundation("PIF", "Bakulina", 2000);
        organizations[4] = new CreditUnion("CreditUnion", "Vesnina");
        organizations[5] = new CreditCafe("CreditCafe", "Beketova");
        organizations[6] = new PawnShop("PawnShop", "Pobedy");
        return organizations;


    }
*/

