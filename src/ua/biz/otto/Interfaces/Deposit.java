package ua.biz.otto.Interfaces;

public interface Deposit {
    void deposit(int time, int amount);
}
