package ua.biz.otto.Interfaces;

public interface Exchange {
    double convert(int currency, int amount);
}
